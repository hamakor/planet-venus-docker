rebuild-docker:
	docker compose build

rebuild:
	git pull --ff-only
	$(MAKE) rebuild-docker

run:
	docker compose run --rm planet
