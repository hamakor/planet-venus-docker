FROM ubuntu:bionic

LABEL org.opencontainers.image.authors="Yehuda Deutsch <yeh@uda.co.il>"

WORKDIR /planet
ARG USER_ID=1000
ARG GROUP_ID=1000

RUN apt-get update -q \
    && DEBIAN_FRONTEND=noninteractive apt-get install -qq locales \
    && sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen \
    && dpkg-reconfigure --frontend=noninteractive locales \
    && update-locale LANG=en_US.UTF-8 \
    && apt-get clean \
    && rm -Rf /var/lib/apt/lists/*

ENV LANG=en_US.UTF-8 \
    LC_ALL=en_US.UTF-8 \
    LANGUAGE=en_US:en

RUN apt-get update -q \
    && apt-get install -qq \
        python-pip \
        planet-venus \
    && apt-get clean \
    && rm -Rf /var/lib/apt/lists/* \
    && pip2 install --no-cache-dir 'html5lib<0.99999999' \
    && groupadd --gid $GROUP_ID planet \
    && useradd -m -s /bin/bash --uid $USER_ID --gid $GROUP_ID planet \
    && mkdir -p /planet/cache \
    && chown -R planet:planet /planet

USER planet
COPY --chown=planet:planet planet-venus/ /planet/

CMD [ "/usr/bin/planet", "/planet/planet.ini" ]
